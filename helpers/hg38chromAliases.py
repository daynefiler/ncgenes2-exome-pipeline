#!/usr/bin/env python

import sys
import argparse
import re
import collections
import gzip
import os.path


# will make multi-level hash of system :: alias that goes to preferred name in that system
aliases = {
        'ensembl': dict(),
        'genbank': dict(),
        'refseq': dict(),
        'ucsc': dict(),
}

# data is from http://hgdownload.soe.ucsc.edu/goldenPath/hg38/database/chromAlias.txt.gz
for line in gzip.open(os.path.join(os.path.dirname(__file__), 'chromAlias.hg38.txt.gz')):
    (name, ucsc_name, systems) = line.decode('utf8').rstrip().split('\t')
    for system in systems.split(','):
        aliases[system][ucsc_name] = name
        aliases['ucsc'][name] = ucsc_name

# chromAlias.txt.gz does not map to ensembl's Y (I think because ensembl's Y masks the PAR)
aliases['ucsc']['Y'] = 'chrY'
aliases['ensembl']['chrY'] = 'Y'

for name, ucsc_name in aliases['ucsc'].items():
    for system in aliases.keys():
        if system == 'ucsc':
            continue
        try:
            aliases[system][name] = aliases[system][ucsc_name]
        except:
            pass


contig_pattern = re.compile(r'##contig=<ID=([^,]+),length=(\d+)>')

parser = argparse.ArgumentParser(description="Change chromosome names to/from accessions")
parser.add_argument('--column', '-c', action='store', default='1', help='column (1-based) to look for the chromosome in, default = 1')
parser.add_argument('--delimiter', '-d', action='store', default='\t', help='column delimiter, default is <TAB>')
parser.add_argument('--format', '-f', action='store', help='format of file', choices=('vcf', 'fasta', 'tsv', 'csv', 'sam', 'chaintarget', 'chainquery'), default='tsv')
parser.add_argument('--silent', '-s', action='store_true', help='silently pass through identifiers that are not recognized (otherwise throw exception)')
parser.add_argument('--target', '-t', action='store', default='refseq', help='target name type (%s)'%(', '.join(sorted(aliases.keys()))))

args = parser.parse_args()
column = int(args.column) - 1
delim = args.delimiter
try:
    translate = aliases[args.target]
except KeyError:
    raise Exception('"%s" is not a recognized system of identifiers (try one of: [%s])'%(args.target, ', '.join(sorted(aliases.keys()))))

def handle_key_error(key, line):
    if args.silent:
        sys.stdout.write(line)
    else:
        raise Exception('identifier "%s" not found for %s'%(key, args.target))

if args.format == 'sam':
    column = 2
elif args.format == 'chaintarget':
    column = 2
    delim = ' '
elif args.format == 'chainquery':
    column = 7
    delim = ' '

if args.format == 'fasta':
    for line in sys.stdin:
        if line.startswith('>'):
            old_identifier = line[1:].split()[0]
            try:
                new_identifier = translate[old_identifier]
                sys.stdout.write('>')
                sys.stdout.write(new_identifier)
                sys.stdout.write(line[(len(old_identifier)+1):])
            except KeyError:
                handle_key_error(old_identifier, line)
        else:
            sys.stdout.write(line)

else: # everything else besides FASTA
    for line in sys.stdin:
        if args.format == 'vcf':
            if line.startswith('#'):
                if line.startswith('##contig=<'):
                    m = contig_pattern.match(line)
                    if m:
                        g = m.groups()
                        try:
                            line = '##contig=<ID=%s,length=%s>\n'%(translate[g[0]], g[1])
                        except KeyError:
                            handle_key_error(g[0], line)
                            continue
                sys.stdout.write(line)
                continue

        if args.format == 'sam':
            if line.startswith('@SQ'):
                sys.stdout.write('@SQ')
                for f in line.rstrip('\n').split('\t'):
                    sys.stdout.write('\t')
                    if f.startswith('SN:'):
                        try:
                            seq = translate[f[3:]]
                        except KeyError:
                            handle_key_error(f[3:], line)
                        sys.stdout.write('SN:')
                        sys.stdout.write(seq)
                    else:
                        sys.stdout.write(f)
                sys.stdout.write('\n')
                continue
            elif line.startswith('@'):
                sys.stdout.write(line)
                continue

        if args.format.startswith('chain') and not line.startswith('chain'):
            sys.stdout.write(line)
            continue

        row = line.rstrip('\n').split(delim)
        try:
            row[column] = translate[row[column]]
            # handle the contig for the paired sequence in sam
            if 'sam' == args.format and row[6] != '=':
                row[6] = translate[row[6]]
        except IndexError:
            sys.stdout.write(line)
            continue
        except KeyError:
            if 'sam' == args.format:
                handle_key_error('%s or %s'%(row[column], row[6]), line)
            else:
                handle_key_error(row[column], line)
            continue
        sys.stdout.write(delim.join(row))
        sys.stdout.write('\n')


sys.stdout.flush()
