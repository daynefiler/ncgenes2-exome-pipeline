#!/bin/bash

: ${GENOME:=/projects/sequence_analysis/vol4/bpow/references/GCF_000001405.26_GRCh38_genomic.fna}
: ${TARGETS:=/projects/sequence_analysis/vol4/bpow/references/agilent_v6_capture_region_pm_100.shortid.38.nooverlap.bed}
: ${IDENTITY_CHECK_BED:=/projects/sequence_analysis/vol4/bpow/references/ic_snp.v2.38.bed}
: ${PIPELINE_TMP:=/scratch/bpow}


IN_SAMPLE=$1
IN_FASTQ1=$2
IN_FASTQ2=$3
IN_FASTQ3=$4
IN_FASTQ4=$5

# 'strict' mode
STRICT="
set -eEu -o pipefail
shopt -s extdebug
IFS=$'\n\t'
trap 'StrictModeFail $?' ERR
"
eval "$STRICT"

timestamp_log() {
    echo -n "::: "
    date +"%F :: %T : " | tr -d '\n'
    echo $@
}

export -f timestamp_log

export EXOME_AWAIT_JOB=""

bwamem() {
    sbatch \
      --export=ALL --parsable --nodes=1 \
      -o slurm-%j-%x.out \
      --job-name=${FUNCNAME[0]}.$SAMPLE $EXOME_AWAIT_JOB \
      --mem-per-cpu=16000 \
      --time=940 \
      --cpus-per-task=5 \
      <<EOSHELL
#!/bin/bash

$STRICT

module load bwa/0.7.17
module load PicardCommandLine/2.18.1

timestamp_log "Running [ ${FUNCNAME[0]} ] on [ \$(hostname) ]"

bwa mem -t 4 -M ${GENOME} "${FASTQ1}" "${FASTQ2}" 2> ${SAMPLE}.bwamem.log | \
  PicardCommandLine AddOrReplaceReadGroups \
    TMP_DIR=${PIPELINE_TMP} \
    VALIDATION_STRINGENCY=SILENT \
    SORT_ORDER=coordinate \
    MAX_RECORDS_IN_RAM=1000000 \
    RGID="$SAMPLE" \
    RGLB="$SAMPLE" \
    RGPL=Illumina \
    RGPU="Illumina HiSeq 2000" \
    RGSM="${IN_SAMPLE}" \
    RGCN=UNC \
    CREATE_INDEX=true \
    INPUT=/dev/stdin \
    OUTPUT="${SAMPLE}.bwamem.bam"

timestamp_log "Completed [ ${FUNCNAME[0]} ] successfully"
EOSHELL
}

fastqc() {
    # args are fastq files
    sbatch \
      --export=ALL --parsable --nodes=1 \
      -o slurm-%j-%x.out \
      --job-name=${FUNCNAME[0]}.$SAMPLE $EXOME_AWAIT_JOB \
      --mem-per-cpu=4000 \
      --time=940 \
      /dev/stdin "$@" <<EOSHELL
#!/bin/bash

$STRICT

module load fastqc/0.11.7

timestamp_log "Running [ ${FUNCNAME[0]} ] on [ \$(hostname) ] with args [ \$@ ]"

mkdir -p fastqc

fastqc -o fastqc "\$@"

timestamp_log "Completed [ ${FUNCNAME[0]} ] successfully"
EOSHELL
}

markdup() {
    sbatch \
      --export=ALL --parsable --nodes=1 \
      -o slurm-%j-%x.out \
      --job-name=${FUNCNAME[0]}.$SAMPLE $EXOME_AWAIT_JOB \
      --mem-per-cpu=17000 \
      --time=940 \
      <<EOSHELL
#!/bin/bash

$STRICT

module load PicardCommandLine/2.18.1

timestamp_log "Running [ ${FUNCNAME[0]} ] on [ \$(hostname) ]"

JAVA_OPTIONS=-Xmx16G PicardCommandLine MarkDuplicates \
  TMP_DIR=${PIPELINE_TMP} \
  VALIDATION_STRINGENCY=SILENT \
  REMOVE_DUPLICATES=false \
  INPUT="${SAMPLE}.bwamem.bam" \
  OUTPUT="${SAMPLE}.bwamem.markdup.bam" \
  METRICS_FILE="${SAMPLE}.bwamem.markdup.metrics" \
  CREATE_INDEX=true

timestamp_log "Completed [ ${FUNCNAME[0]} ] successfully"
EOSHELL
}

call_ic() {
    # args are bam files
    sbatch \
      --export=ALL --parsable --nodes=1 \
      -o slurm-%j-%x.out \
      --job-name=${FUNCNAME[0]}.$SAMPLE $EXOME_AWAIT_JOB \
      --mem-per-cpu=2000 \
      --time=1880 \
      /dev/stdin "$@" <<EOSHELL
#!/bin/bash

$STRICT

# for bgzip, tabix
module load htslib/1.8

module load freebayes/git-2018-02-14

timestamp_log "Running [ ${FUNCNAME[0]} ] on [ \$(hostname) ] with bam files [ $@ ]"

freebayes \
  -f "$GENOME" \
  -t "$IDENTITY_CHECK_BED" \
  --report-monomorphic \
  --genotype-qualities \
  --strict-vcf \
  "\$@" \
 | bgzip > "${SAMPLE}.ic.vcf.gz"

timestamp_log "Completed [ ${FUNCNAME[0]} ] successfully"
EOSHELL
}

freebayes() {
    # args are bam files
    sbatch \
      --export=ALL --parsable --nodes=1 \
      -o slurm-%j-%x.out \
      --job-name=${FUNCNAME[0]}.$SAMPLE $EXOME_AWAIT_JOB \
      --mem-per-cpu=2000 \
      --time=1880 \
      /dev/stdin "$@" <<EOSHELL
#!/bin/bash

$STRICT

# for bgzip, tabix
module load htslib/1.8

module load freebayes/git-2018-02-14

timestamp_log "Running [ ${FUNCNAME[0]} ] on [ \$(hostname) ]"

freebayes \
  -f "$GENOME" \
  -t "$TARGETS" \
  --genotype-qualities \
  --strict-vcf \
  "\$@" \
 | bgzip > "${SAMPLE}.fb.vcf.gz"

timestamp_log "creating vcf index"
tabix -p vcf "${SAMPLE}.fb.vcf.gz"

timestamp_log "Completed [ ${FUNCNAME[0]} ] successfully"
EOSHELL
}

# ---------------------------------------------------
# Now for the pipeline...
# ---------------------------------------------------

export SAMPLE=$IN_SAMPLE
JOBID_FASTQC=$(fastqc $IN_FASTQ1 $IN_FASTQ2 $IN_FASTQ3 $IN_FASTQ4)
echo "submitted FASTQC: $JOBID_FASTQC"

export FASTQ1=$IN_FASTQ1
export FASTQ2=$IN_FASTQ2
export SAMPLE=${IN_SAMPLE}.0
JOBID_BWA_A=$(bwamem)
echo "submitted BWA: $JOBID_BWA_A"
export EXOME_AWAIT_JOB="--dependency=afterok:${JOBID_BWA_A}"
JOBID_MARKDUP_A=$(markdup)
export EXOME_AWAIT_JOB="--dependency=afterok:${JOBID_MARKDUP_A}"
JOBID_IC_A=$(call_ic ${SAMPLE}.bwamem.markdup.bam)
echo "submitted ic $JOBID_IC_A"

export FASTQ1=$IN_FASTQ3
export FASTQ2=$IN_FASTQ4
export SAMPLE=${IN_SAMPLE}.1
export EXOME_AWAIT_JOB=""
JOBID_BWA_B=$(bwamem)
echo "submitted BWA: $JOBID_BWA_B"
export EXOME_AWAIT_JOB="--dependency=afterok:${JOBID_BWA_B}"
JOBID_MARKDUP_B=$(markdup)
export EXOME_AWAIT_JOB="--dependency=afterok:${JOBID_MARKDUP_B}"
JOBID_IC_B=$(call_ic ${SAMPLE}.bwamem.markdup.bam)
echo "submitted ic $JOBID_IC_B"

export SAMPLE=$IN_SAMPLE
export EXOME_AWAIT_JOB="--dependency=afterok:${JOBID_MARKDUP_A},${JOBID_MARKDUP_B}"
JOBID_FREEBAYES=$(freebayes ${SAMPLE}.0.bwamem.markdup.bam ${SAMPLE}.1.bwamem.markdup.bam)
echo "submitted freebayes $JOBID_FREEBAYES"
# after freebayes returns, report status as completed
