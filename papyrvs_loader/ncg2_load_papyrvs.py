#!/usr/bin/env python3


import pysam
import psycopg2
import psycopg2.extras
import json
import sys
import os
import subprocess
import csv
import gzip
import math
import collections
import urllib.request
import urllib.parse
import argparse
import re

# FIXME: this should be read from something defined by the snakemake file...
PIPELINE_VERSION = '20190228'
DEFAULT_WEBSERVICE = 'https://ws.ncg2.unc.edu/EndPoints.svc/UpdateSampleStatus/{token}/{provisionalParticipantId}/{callsetId}/{statusTypeId}'
WEBSERVICE_DEBUG_TOKEN = '5795f877-500b-4f72-b73c-087246bbbd7c'
WEBSERVICE_QC_START_CODE = '160' # defined in the webservices code

try:
    from urllib import unquote  # Python 2.X
except ImportError:
    from urllib.parse import unquote  # Python 3+


annotations_description='Allele|Annotation|Annotation_Impact|Gene_Name|Gene_ID|Feature_Type|Feature_ID|Transcript_BioType|Rank|HGVS.c|HGVS.p|cDNA.pos|CDS.pos|AA.pos|Distance|WARN'.split("|")

create_temp_table = """
CREATE TEMP TABLE load_calls (
    callset_id              INTEGER,
    var_id                  BIGINT,
    qual                    DOUBLE PRECISION,
    filter                  TEXT, -- maybe change PASS to null for space reasons
    ann                     jsonb,
    gene                    text,
    priority                integer,
    functional_anno         text,
    annotation_impact       text,
    --info              jsonb,
    clinvar_var_id          text,
    scv_with_criteria       text,
    asserted_pathogenic     boolean,
    overall_af              DOUBLE PRECISION,
    popmax_af               DOUBLE PRECISION,
    popmax                  text,
    genotype                text,
    depth                   integer,
    allele_depths           integer[],
    genotype_qual           integer,
    likelihoods             integer[]
);
"""

values_sql = """
INSERT INTO load_calls (
    var_id, qual, filter,
    ann, gene, priority, functional_anno, annotation_impact, 
    clinvar_var_id, scv_with_criteria, asserted_pathogenic, overall_af, popmax_af, popmax,
    genotype, depth, allele_depths, genotype_qual, likelihoods)
VALUES %s
"""
values_template = """
(
    variant_analysis.get_id_for_normvar(%(seq)s, %(pos)s, %(ref)s, %(alt)s), %(qual)s, %(filter)s,
    %(annotations)s, %(gene)s, %(priority)s, %(functional_anno)s, %(annotation_impact)s,
    %(clinvar_var_id)s, %(scv_with_criteria)s, %(clinvar_asserted_pathogenic)s, %(gnomad_af)s, %(gnomad_af_popmax)s, %(gnomad_popmax)s,
    %(genotype)s, %(depth)s, %(allele_depths)s, %(genotype_qual)s, %(likelihoods)s
)
"""

def variants2records(variants, feature_filter = None):
    for variant in variants:
        info = variant.info
        for i, alt in enumerate(variant.alts):
            output = {
                'seq': variant.chrom,
                'pos': variant.pos,
                'ref': variant.ref,
                'alt': alt,
                'qual': variant.qual,
                'filter': ','.join(variant.filter)
            }
            if 'GNOMAD_AF' in info and not None in info['GNOMAD_AF']:
                output.update(
                    gnomad_af = info['GNOMAD_AF'][i-1],
                    gnomad_af_popmax = info['GNOMAD_AF_POPMAX'][i-1],
                    gnomad_popmax = info['GNOMAD_POPMAX'][i-1])
            else:
                output.update(
                    gnomad_af = 0,
                    gnomad_af_popmax = 0,
                    gnomad_popmax = None)
            
            output.update(
                clinvar_var_id = info.get('clinvar_variation_id'),
                clinvar_asserted_pathogenic = info.get('clinvar_asserted_pathogenic', 0) == '1',
                scv_with_criteria = info.get('scv_with_criteria'),
            )
            
            ann = info.get('ANN', [])
            if isinstance(ann, str):
                ann = (ann,)
            annotations = [dict(zip(annotations_description, (unquote(x) for x in a.split('|')))) for a in ann]
            if feature_filter is not None:
                annotations = [a for a in annotations if re.search(feature_filter, a['Feature_ID'])]
            output.update(annotations = psycopg2.extras.Json(annotations))
            worst_anno = annotations[0] if len(annotations) > 0 else {}
            
            priority = 5
            if output['clinvar_asserted_pathogenic']:
                priority = 1
            elif (output['gnomad_af'] <= 0.01):
                impacts = [x['Annotation_Impact'] for x in annotations]
                if "HIGH" in impacts:
                    priority = 2
                elif "MODERATE" in impacts:
                    priority = 3
                else:
                    priority = 4
            
            sample_gt = variant.samples[0]
            output.update(
                priority = priority,
                # you might think '/'.join(sample_gt.alleles) would work, but freebayes sometimes has '1/0' alleles
                genotype = '/'.join(variant.alleles[i] for i in sorted(sample_gt.allele_indices)),
                depth = sample_gt.get('DP'),
                allele_depths = list(sample_gt.get('AD')),
                genotype_qual = sample_gt.get('GQ'),
                likelihoods = list(sample_gt.get('GL')),
                gene = worst_anno.get('Gene_Name'),
                functional_anno = worst_anno.get('Annotation'),
                annotation_impact = worst_anno.get('Annotation_Impact'),
            )
            yield output

def exonCoveragestat2dbrow(entry, callset_id):
    return [
        callset_id,
        int(entry['start0']),
        int(entry['end']),
        int(entry['region_index']),
    ] + [entry.get('pct_gte_%d'%depth) for depth in (1,5,10,20,50)] + [
        float(entry['coverage_mean']),
        float(entry['coverage_median']),
        int(entry['coverage_min']),
        int(entry['coverage_max']),
        entry['transcript_id'],
    ]

def transcriptCoveragestat2dbrow(entry, callset_id):
    return [
        callset_id,
    ] + [entry.get('pct_gte_%d'%depth) for depth in (1,5,10,20,50)] + [
        float(entry['coverage_mean']),
        float(entry['coverage_median']),
        int(entry['coverage_min']),
        int(entry['coverage_max']),
        entry['#transcript_id'],
        entry['gene'],
        entry['protein_id'],
    ]

def check_bam_readgroups(from_filename, bam_file):
    read_groups = []
    sam_header_process = subprocess.run('''module load samtools; samtools view -H "%s"'''%bam_file,
        check=True, encoding='utf-8', shell=True, stdout=subprocess.PIPE)
    for line in sam_header_process.stdout.split('\n'):
        if line.startswith('@RG'):
            for col in line.rstrip().split('\t'):
                if col.startswith('ID:'):
                    read_groups.append(col[3:])

    if set(read_groups) != set(readgroups_from_filename):
        raise RuntimeException("readgroups from filename do not match @RG from bam file:\n  @RG: %s\n  from filename: %s"%(read_groups, readgroups_from_filename))

def metrics_data_block_records(lines):
    headers = next(lines).rstrip('\n').split('\t')
    for line in lines:
        if line == '\n':
            return
        yield collections.OrderedDict(zip(headers, line.rstrip('\n').split('\t')))


def read_metrics(lines, metrics_of_interest = 'picard.analysis.directed.HsMetrics'):
    for line in lines:
        if line.startswith('## METRICS CLASS\t'):
            metrics_class = line.rstrip().split('\t')[1]
            if metrics_class in metrics_of_interest:
                return [x for x in metrics_data_block_records(lines)]


def deliveryStatusForSample(samplename, project_code, api_key):
    params = urllib.parse.urlencode({'query.sampleName': samplename})
    req = urllib.request.Request('https://tracseq.genomics.unc.edu/api/DeliveryStatus?%s'%params,
            headers={'X-TRACSEQ-PROJECT-CODE': project_code, 'X-TRACSEQ-API-KEY': api_key})
    return json.load(urllib.request.urlopen(req))


def load_callset(cur, provisional_sample, variant_file, sex_heuristic_file, read_groups):
    vf = pysam.VariantFile(variant_file)

    with open(sex_heuristic_file) as shf:
        inferred_sex = shf.readline().rstrip()

    cur.execute("INSERT INTO sample_analysis.callset (provisional_sample, read_groups, pipeline_version, inferred_sex) VALUES (%s, %s, %s, %s) RETURNING callset_id",
        (provisional_sample, read_groups, PIPELINE_VERSION, inferred_sex))
    callset_id = cur.fetchone()[0]

    cur.execute(create_temp_table)

    psycopg2.extras.execute_values(cur, sql=values_sql, argslist=(x for x in variants2records(vf, args.feature_filter)), template=values_template, page_size=10000)

    cur.execute("""
    INSERT INTO sample_analysis.callset_call
    SELECT
        %s as callset_id,
        var_id,
        FALSE AS discuss,
        FALSE AS verify,
        FALSE AS suspected_artifact,
        qual, filter,
        ann, gene, priority, functional_anno, annotation_impact, 
        clinvar_var_id, scv_with_criteria, asserted_pathogenic, overall_af, popmax_af, popmax,
        genotype, depth, allele_depths, genotype_qual, likelihoods
    FROM load_calls;
    """, (callset_id,))

    print("inserted %d records"%(cur.rowcount))
    return callset_id


def load_identity_check(cur, provisional_sample, callset_id, identity_check_file):
    with pysam.VariantFile(identity_check_file) as ic_vars:
        if ic_vars.header.samples[0] != provisional_sample:
            raise RuntimeException("%s is name of sample in %s, expected %s"%(ic_vars.header.samples[0], identity_check_file, provisional_sample))
        for v in ic_vars:
            genotype = '/'.join(v.samples[0].alleles)
            depth = v.samples[0]['DP']
            allele_depths = list(v.samples[0]['AD'])
            # hacky -- works for autosomes
            chrom = str(math.floor(float(v.chrom[3:])))
            cur.execute('''
                INSERT INTO sample_analysis.identity_check (callset_id, identity_check_site_id, genotype, depth, allele_depths)
                VALUES (%s, (SELECT identity_check_site_id FROM sample_analysis.identity_check_site WHERE chrom=%s AND pos38=%s), %s, %s, %s)
                ''', (callset_id, chrom, v.pos, genotype, depth, allele_depths))


def load_coverages(cur, callset_id, exon_coverage_file, transcript_coverage_file):
    psycopg2.extras.execute_values(cur,
        sql = 'INSERT INTO sample_analysis.cds_exon_depth_of_coverage VALUES %s',
        argslist=(exonCoveragestat2dbrow(x, callset_id) for x in csv.DictReader(gzip.open(exon_coverage_file, 'rt'), dialect='excel-tab'))
    )

    psycopg2.extras.execute_values(cur,
        sql = 'INSERT INTO sample_analysis.transcript_depth_of_coverage VALUES %s',
        argslist=(transcriptCoveragestat2dbrow(x, callset_id) for x in csv.DictReader(gzip.open(transcript_coverage_file, 'rt'), dialect='excel-tab'))
    )


def load_qc_data(cur, provisional_sample, callset_id, hsmetrics_filename, readgroup_tracseq_stats):
    ## FIXME! There will be some tracseq changes needed:
    ## - add additional data (e.g., % perfect index)
    ## - keep up with readgroup names


    # the capitalized names below are the column headers from output of CollectHsMetrics
    insert_exome_qc_sql = '''
    INSERT INTO sample_analysis.exome_qc
        (callset_id, read_groups, cluster_pct, q30_pct, perfect_index_pct, read_count, mean_target_coverage, median_target_coverage,
         percent_unique, percent_mapped, percent_1x, percent_20x)
    VALUES
        (%(callset_id)s, %(READ_GROUP)s, %(percentOfRawClusters)s, %(q30Bases)s, %(percentPerfectIndexReads)s,
         %(PF_READS)s, %(MEAN_TARGET_COVERAGE)s, %(MEDIAN_TARGET_COVERAGE)s,
         %(PCT_PF_UQ_READS)s, %(PCT_PF_UQ_READS_ALIGNED)s, %(PCT_TARGET_BASES_1X)s, %(PCT_TARGET_BASES_20X)s)'''



    with open(hsmetrics_filename) as unparsed:
        metrics = read_metrics(unparsed)

    # TODO: check this against RG from bam file...
    all_readgroups = sorted([m['READ_GROUP'] for m in metrics if m['READ_GROUP'] != '' and m['SAMPLE'] == provisional_sample])

    for m in metrics:
        if m['SAMPLE'] == provisional_sample:
            m['callset_id'] = callset_id
            if m['READ_GROUP'] == '':
                if len(all_readgroups) == 1:
                    # no need for a merged record if there is only one readgroup present...
                    continue
                m['READ_GROUP'] = all_readgroups
                # TODO: consider averaging across flowcells...
                m['percentOfRawClusters'] = None
                m['q30Bases'] = None
                m['percentPerfectIndexReads'] = None
            else:
                try:
                    m.update(readgroup_tracseq_stats[m['READ_GROUP']])
                except KeyError:
                    m['percentOfRawClusters'] = None
                    m['q30Bases'] = None
                    m['percentPerfectIndexReads'] = None
                m['READ_GROUP'] = [m['READ_GROUP']]
            cur.execute(insert_exome_qc_sql, m)

    

if '__main__' == __name__:
    parser = argparse.ArgumentParser(description="load calls and qc into papyrvs")
    parser.add_argument('basename', nargs=1, help="basename of the file ({samplename}.{+-joined-readgroups}")
    parser.add_argument('-i', '--ignore-tracseq', default=False, action='store_true')
    parser.add_argument('-b', '--ignore-bam-readgroup-check', default=False, action='store_true')
    parser.add_argument('-w', '--webservice-notification', default=False, action='store_true', help='Notify webservice of completion')
    parser.add_argument('-f', '--feature-filter', help='Regular expression by which to filter the "Feature_ID" (usually transcript name; try "^NM_")')

    args = parser.parse_args()

    # callset_base is '%s.%s'%(samplename, '+'.join(readgroups)) ...
    # basically, it is the basename of the bam file minus '.bam'
    callset_base = args.basename[0]
    (samplename, plus_readgroups) = callset_base.split('.', 1)
    readgroups_from_filename = plus_readgroups.split('+')
    variant_file = 'annotated-filtered/%s.fb.splitmulti.clinvar.gnomad.bcf'%callset_base
    identity_check_file = 'identity_check/%s.ic.vcf.gz'%callset_base
    exon_coverage_file = 'markdup/%s.exon.coveragestats.tsv.gz'%callset_base
    transcript_coverage_file = 'markdup/%s.transcript.coveragestats.tsv.gz'%callset_base
    sex_heuristic_file = 'markdup/%s.sexheuristic'%callset_base
    bam_file = 'markdup/%s.bam'%callset_base
    hsmetrics_file = 'markdup/%s.hsmetrics'%callset_base

    if not args.ignore_bam_readgroup_check:
        check_bam_readgroups(readgroups_from_filename, bam_file)

    readgroup_tracseq_stats = {}
    if not args.ignore_tracseq:
        # check now for needed environment variables so we don't do anything else
        try:
            tracseq_project_code = os.environ['TRACSEQ_PROJECT_CODE']
            tracseq_api_key = os.environ['TRACSEQ_API_KEY']
        except KeyError:
            raise RuntimeError('The TRACSEQ_API_KEY and TRACSEQ_PROJECT_CODE variables must be set in ' +
                'order to get data from TracSeq... or just set "-i" to only write HsMetrics')
        deliveryStatus = deliveryStatusForSample(samplename, tracseq_project_code, tracseq_api_key)

        # FIXME: consider whether this query can return more than one item in demultiplexedSamples
        for ds in deliveryStatus:
            if len(ds['demultiplexedSamples']) == 1:
                readgroup_tracseq_stats['%s_%s_L%03d'%(samplename, ds['runId'], ds['lane'])] = ds['demultiplexedSamples'][0]
            elif len(ds['demultiplexedSamples']) == 0:
                print("%s did not have any demultiplexed samples, may have been a failed run in TracSeq"%ds)
            else:
                raise RuntimeError('Expected one demuxedsamples per sample, runId, lane, but got %d'%len(ds['demultiplexedSamples']))

    try:
        dsn = os.environ['PGDSN']
        print('using connection requested in PGDSN: "%s"'%dsn)
    except KeyError:
        print("using default database connection (override with PGDSN environment variable, if you prefer)")
        dsn = "dbname=papdev host=genomicsdb.renci.org user=bcpowell"

    con = psycopg2.connect(dsn)
    cur = con.cursor()

    callset_id = load_callset(cur, samplename, variant_file, sex_heuristic_file, sorted(readgroups_from_filename))
    load_identity_check(cur, samplename, callset_id, identity_check_file)
    load_coverages(cur, callset_id, exon_coverage_file, transcript_coverage_file)
    load_qc_data(cur, samplename, callset_id, hsmetrics_file, readgroup_tracseq_stats)


    con.commit()
    con.close()

    # notify the WFE webservice that we have a sample ready for QC
    if args.webservice_notification:
        try:
            token = os.environ['NCG2_WEBSERVICE_TOKEN']
        except KeyError:
            print("No webservice token provided, using the 'debug' token")
            token = NCG2_WEBSERVICE_DEBUG_TOKEN
        try:
            res = urllib.request.urlopen(DEFAULT_WEBSERVICE.format(token=token,
                provisionalParticipantId=samplename, callsetId=callset_id,
                statusTypeId=WEBSERVICE_QC_START_CODE))
        except urllib.request.HTTPError as err:
            print("Error with notifying webservice!")
            print('\n'.join(x.decode('utf-8') for x in err))
            raise err # re-raise now that we printed out some extra info
        if res.getcode() != 200:
            raise RuntimeError('response code from webservice was %d, should have been 200'%res.getcode())
        response_text = '\n'.join(x.decode('utf-8') for x in res)
        if not response_text.lstrip().startswith('"Success"'):
            raise RuntimeError("webservice call did not report success:\n%s"%response_text)

